$(document).ready(function() {
    $('#da-thumbs > li').hoverdir();
    var initial_items = 6;
    var next_items = 3;
    var $grid = $('.grid').isotope({
        itemSelector: '.element-item',
        layoutMode: 'fitRows',
        stamp: '.element-item--static'

    });

    // change is-checked class on buttons

    function showNextItems(pagination) {
        var itemsMax = $('.projVisibleItem').length;
        var itemsCount = 0;
        $('.projVisibleItem').each(function () {
            if (itemsCount < pagination) {
                $(this).removeClass('projVisibleItem');
                itemsCount++;
            }
        });
        if (itemsCount >= itemsMax) {
            $('#showMoreProj').hide();
        }
        $grid.isotope('layout');
    }

    // function that hides items when page is loaded
    function hideItems(pagination) {
        var itemsMax = $('.element-item').length;
        var itemsCount = 0;
        $('.element-item').each(function () {

            if (itemsCount >= pagination) {
                $(this).addClass('projVisibleItem');
            }
            itemsCount++;
        });
        if (itemsCount < itemsMax || initial_items >= itemsMax) {
            $('#showMoreProj').hide();
        }
        $grid.isotope('layout');
    }

    $('#showMoreProj').on('click', function (e) {
        e.preventDefault();
        showNextItems(next_items);
    });
    hideItems(initial_items);
});
