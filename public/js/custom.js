$(document).ready(function () {

    var $carousel = $('.carousel');

    $carousel
        .slick({
            // centerMode: true,
            adaptiveHeight: true,
            arrows: true,
            variableHeight: false,
            nextArrow: '<i class="fas fa-angle-right fa-3x arrow-right"></i>',
            prevArrow: '<i class="fas fa-angle-left fa-3x arrow-left"></i>',
            autoplay: true,
            autoplaySpeed: 3000,
        })
        .magnificPopup({
            type: 'image',
            delegate: 'a:not(.slick-cloned)',
            gallery: {
                enabled: true
            },
            callbacks: {
                open: function () {
                    var current = $carousel.slick('slickCurrentSlide');
                    $carousel.magnificPopup('goTo', current);
                },
                beforeClose: function () {
                    $carousel.slick('slickGoTo', parseInt(this.index));
                }
            }
        });


    $('#send').click(function (e) {
        e.preventDefault();

        var params = $('#contact-form').serialize();

        $.ajax({
            url: '/send',
            data: params,
            type: 'POST',
            success: function () {
                $('#contact-form button').text('Ваше сообщение отправлено').addClass('sendMessage');
                setTimeout(function () {
                    $('#contact-form button').text('Отправить').removeClass('sendMessage');
                }, 3000)
            },
            error: function (data) {
                $.each(data.responseJSON.errors, function (key, value) {
                    $("#" + key).text(value).addClass("error");
                    if (key == 'policy')
                        $('.control_indicator').addClass('error');
                    setTimeout(function () {
                        $("#" + key).text(value).removeClass('error');
                        $('.control_indicator').removeClass('error')
                    }, 2500)
                })
            }
        });
    })
});
