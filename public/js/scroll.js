$(document).ready(function(){
    let path = window.location.pathname;
    let origin = window.location.origin;
    let hash  = window.location.hash;
    // при переходе из проекто в или блога проверяется есть ли хэш и если есть
    // то делает пункт меню активным
    if (hash)
    {
        $(".anchor").each(function () {
            if (hash == '#' + $(this).attr('href'))
                $(this).addClass('active');
        });
    }

    if (path != '/')
    {
        $(".anchor").each(function () {
            $(this).attr('href', origin + $(this).attr('href'));

        })
    }
    else
    {

        // добавлет # перед ссылками в меню, если это главная страница
        // $(".anchor").each(function () {
        //     $(this).attr('href', '#' + $(this).attr('href'));
        // })

        // Add smooth scrolling to all links
        $("nav a").on('click', function(event) {
            let currentLink = this.hash;

            $(".anchor").each(function () {
                if (this.hash == currentLink)
                    $(this).addClass('active');
                else
                    $(this).removeClass('active');
            });

            // Make sure this.hash has a value before overriding default behavior

            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 500, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    }


});