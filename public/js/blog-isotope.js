$(document).ready(function() {
    $('#da-thumbs > li').hoverdir();
    var initial_items = 6;
    var next_items = 3;
    var $grid2 = $('.grid2').isotope({
        itemSelector: '.blog-item',
        layoutMode: 'fitRows',
        stamp: '.blog-item--static'

    });

    // change is-checked class on buttons

    function showNextItems(pagination) {
        var itemsMax = $('.blogVisibleItem').length;
        var itemsCount = 0;
        $('.blogVisibleItem').each(function () {
            if (itemsCount < pagination) {
                $(this).removeClass('blogVisibleItem');
                itemsCount++;
            }
        });
        if (itemsCount >= itemsMax) {
            $('#showMoreBlog').hide();
        }
        $grid2.isotope('layout');
    }

    // function that hides items when page is loaded
    function hideItems(pagination) {
        var itemsMax = $('.blog-item').length;
        var itemsCount = 0;
        $('.blog-item').each(function () {

            if (itemsCount >= pagination) {
                $(this).addClass('blogVisibleItem');
            }
            itemsCount++;
        });
        if (itemsCount < itemsMax || initial_items >= itemsMax) {
            $('#showMoreBlog').hide();
        }
        $grid2.isotope('layout');
    }

    $('#showMoreBlog').on('click', function (e) {
        e.preventDefault();
        showNextItems(next_items);
    });
    hideItems(initial_items);
});
