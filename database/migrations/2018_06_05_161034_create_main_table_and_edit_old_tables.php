<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMainTableAndEditOldTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('main', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img')->nullable();
            $table->string('gallery')->nullable();
            $table->string('text')->nullable();
            $table->string('fb_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
        });

        Schema::table('projects', function (Blueprint $table) {
           $table->string('pictograms');
        });
        Schema::rename('pictogram', 'services');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('main');
        Schema::table('projects', function (Blueprint $table){
            $table->drop('pictograms');
        });
        Schema::table('services')->rename('pictograms');
    }
}
