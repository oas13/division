<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDecriptionColumnInServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('services', function (Blueprint $table) {
            $table->text('description');
        });
        Schema::table('main', function (Blueprint $table) {
            $table->text('yt_link');
            $table->text('yt_icon');
            $table->text('fb_icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('description');
        });
        Schema::table('main', function (Blueprint $table) {
            $table->dropColumn('yt_link');
            $table->dropColumn('yt_icon');
            $table->dropColumn('fb_icon');
        });
    }
}
