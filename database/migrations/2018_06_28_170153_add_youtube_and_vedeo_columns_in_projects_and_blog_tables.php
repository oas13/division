<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYoutubeAndVedeoColumnsInProjectsAndBlogTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('projects', function (Blueprint $table) {
            $table->text('youtube_src')->nullable();
            $table->boolean('toggle_youtube')->default(0);
            $table->text('video_src')->nullable();
            $table->boolean('toggle_video')->default(0);
        });

        Schema::table('blog', function (Blueprint $table) {
            $table->text('youtube_src')->nullable();
            $table->boolean('toggle_youtube')->default(0);
            $table->text('video_src')->nullable();
            $table->boolean('toggle_video')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('projects', function (Blueprint $table) {
           $table->drop('youtube_srtc', 'toggle_youtube', 'video_src', 'toggle_video');
        });
        Schema::table('blog', function (Blueprint $table) {
           $table->drop('youtube_srtc', 'toggle_youtube', 'video_src', 'toggle_video');
        });
    }
}
