<?php

namespace App\Http\Controllers;

use App\Models\Projects;

	class ProjectController
	{
		public function index($slug)
        {
            $project = Projects::where('slug', $slug)->first();

            return view('project', compact('project'));
        }
	}