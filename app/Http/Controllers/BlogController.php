<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 03.06.2018
 * Time: 18:50
 */

namespace App\Http\Controllers;

use App\Models\Blog;
class BlogController
{

    public function index($slug)
    {
        $topic = Blog::where('slug', $slug)->first();

        return view('topic', compact('topic'));
    }
}