<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 02.06.2018
 * Time: 22:15
 */

namespace App\Http\Controllers;

use App\Http\Requests\StoreContact;
use App\Mail\SendMail;
use App\Models\Team;
use App\Models\Service;
use App\Models\Projects;
use App\Models\Blog;
use App\Models\Main;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class MainController
{
    protected $team;
    protected $services;
    protected $projects;
    protected $blog;
    public $request;

    public function __construct(Team $team, Service $services, Projects $projects, Blog $blog, Request $request)
    {
        $this->team = $team;
        $this->services = $services;
        $this->projects = $projects;
        $this->blog = $blog;
        $this->request = $request;
    }

    public function index()
    {
        $teamUsers = $this->team->all();
        $services = $this->services->all();
        $projects = $this->projects->all();
        $blog = $this->blog->all();
        $main = Main::first();

        return view('main', compact(['teamUsers', 'services', 'projects', 'blog', 'main']));
    }

    public function sendMail(StoreContact $request)
    {
        $name = $request->name;
        $email = $request->email;
        $msg = $request->message;

        Mail::to('oas90@bk.ru')->send(new SendMail($name, $email, $msg));
    }
}