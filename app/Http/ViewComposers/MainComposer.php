<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 01.06.2018
 * Time: 0:46
 */
namespace App\Http\ViewComposers;

use App\Models\NavItem;
use App\Models\Main;

use Illuminate\View\View;
class MainComposer
{
    protected $menu;
    protected $main;

    public function __construct(NavItem $menu, Main $main)
    {
        // Dependencies automatically resolved by service container...
        $this->menu = $menu;
        $this->main = $main;

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menuItems = $this->menu->all();
        $main = $this->main->first();

        $view->with('menuItems', $menuItems);
        $view->with('main', $main);
    }
}