<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');
Route::get('projects/{slug}', 'ProjectController@index');
Route::get('blog/{slug}', 'BlogController@index');
Route::post('/send', 'MainController@sendMail');
Route::group(['prefix' => 'admin', 'middleware' => ['admin'], 'namespace' => 'Admin'], function()
{
   CRUD::resource('main', 'MainCrudController');
   CRUD::resource('navitems', 'NavItemsCrudController');
   CRUD::resource('services', 'PictogramCrudController');
   CRUD::resource('projects', 'ProjectsCrudController');
   CRUD::resource('team', 'TeamCrudController');
   CRUD::resource('blog', 'BlogCrudController');
});
