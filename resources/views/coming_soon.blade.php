<!doctype html>
<html>
<head>

    <meta charset="UTF-8">
    <title>Красивая заглушка для сайта</title> <!-- Add your title -->
    <link rel="shortcut icon" href="img/favicon.ico" /> <!-- Add your own favicon to the images folder -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <style>
        .background {
            position: fixed;
            width: 100%;
            height: 100%;
            background: #1F1F21;
            background: rgba(31, 31, 33, 0.25);
            z-index: -1;
        }
        .page-wrapper {
            text-align: center;
            width: 100%;
            height: 100%;
            padding: 24px 0;
        }
        .page-wrapper:before {
            content: '';
            display: inline-block;
            height: 100%;
            vertical-align: middle;
            margin-right: -0.25em; /* Adjusts for spacing */
        }
        .page-wrapper-inner {
            display: inline-block;
            vertical-align: middle;
        }
    </style>

</head>
<body background="{{ asset('img/BMA_9031.jpg') }}" style="background-size: cover">

<div class="background"></div>

<div class="page-wrapper">

    <div class="page-wrapper-inner">

        <div class="main current">
            <div class="container">

                <div class="logo-wrapper">
                    <img class="logo" src="{{ asset('img/Логотип.png') }}" style="width: 500px">
                </div> <!-- end logo-wrapper -->

                <div class="subline-wrapper">
                    <h1 style="color: white; font-family: Comfortaa">Сайт скоро заработает</h1>
                </div><!-- end about-wrapper -->
            </div> <!-- end container -->
        </div> <!-- end main -->
    </div>
</div>

<!-- SCRIPTS -->
<!-- jQuery 1.10.2 -->
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<!-- Bootstrap 3.0.2 -->
<script src="{{ asset('js/bootstrap.min.js')}}"></script>

</body>
</html>