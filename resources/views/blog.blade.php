    <div id="blog" class="section">
        <div class="container-fluid">
            <div class="row grid2">
                @foreach($blog as $topic)
                    <div class="col-sm-1 col-md-6 col-lg-4 blog-item hoverItem">
                        <a href="/blog/{{$topic->slug}}">
                            <img src="{{ asset($topic->image) }}" alt="" class="img-fluid">
                            <div class="back">
                                <p class="hoverTitle">{{ $topic->title }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
            <a class="button showMore" id="showMoreBlog">Посмотреть еще</a>
        </div>
    </div>