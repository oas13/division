<nav class="navbar navbar-light fixed-top navbar-expand-md">

    <a class="navbar-brand logo" href="/">
        <img src="{{ asset($main->logo) }}">
    </a>

    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-collapse collapse" id="navbarCollapse" style="">
        <ul class="navbar-nav ml-auto">
            @foreach($menuItems as $item)
                <li class="">
                    <a class="anchor" href="#{{ $item->link }}">
                        {{ $item->name }}
                    </a>
                </li>
            @endforeach
        </ul>
    </div>

</nav>
