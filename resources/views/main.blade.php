@extends('layout')

@section('content')

    <div id="main-banner" style="background-image: url({{ $main->img }});">
        <div class="col-md-12 blue-line">
            <h2>{{ $main->text }}</h2>
        </div>
    </div>
    <div class="container-fluid">
        @include('services')
        <!-- /.container -->

        @include('projects')

        @include('about', ['about' => $main->text_about])

        @include('blog')

    </div>
    @include('contacts')
@endsection