<div id="contacts" class="section" style="background-image: url({{ $main->contacts_img }})">
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <form class="form" method="post"
                  enctype="multipart/form-data" id="contact-form">
                @csrf
                <div class="form-group">
                    <input type="email" class="form-control" id="name" placeholder="Имя" name="name">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" id="email" placeholder="Email" name="email">
                </div>
                <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="Сообщение" rows="5" name="message"></textarea>
                </div>
                {{--<div class="form-group">--}}
                    {{--<div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE_KEY') }}"></div>--}}
                    {{--<span id="g-recaptcha-response"></span>--}}
                {{--</div>--}}
                <!-- Modal -->
                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Политика конфиденциальности</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {!! $main->policy !!}
                            </div>
                        </div>
                    </div>
                </div>
                <button id="send" class="btn">Отправить</button>
                {{--<div class="policy form-group">--}}
                    {{--<p>--}}
                        {{--Нажимая на кнопку, Вы соглашаетесь на обработку персональных данных в соответствии с--}}
                        {{--<a href="#" data-toggle="modal" data-target="#exampleModalLong">политикой конфиденциальности</a>--}}
                    {{--</p>--}}
                {{--</div>--}}
                <div class="control-group policy">
                    <label class="control control-checkbox">
                        Нажимая на кнопку, Вы соглашаетесь на обработку персональных данных в соответствии с
                        <a href="#" data-toggle="modal" data-target="#exampleModalLong">политикой конфиденциальности</a>
                        <input type="checkbox" checked="checked" name="policy" id="policy">
                        <div class="control_indicator"></div>
                    </label>
                </div>
            </form>
        </div>

        <div class="col-md-4 email">
            <p>
                <b>{{ $main->phone }}
                    <br>
                    {{ $main->email }}
                </b>
            </p>
        </div>
    </div>
    </div>
</div>
