<div id="services" class="section">
    <div class="container-fluid">
        <div class="row">
        @foreach($services as $service)
            <div class="col-6 col-md-6 col-lg-3 pictogram">
                <a href="#" data-toggle="modal" data-target="#{{ $service->id }}">
                    <img src="{{ asset($service->image) }}" alt="" class="img-fluid">
                </a>
                <p>
                    {{ $service->description }}
                </p>
                <div class="modal fade" id="{{ $service->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                {{--<h5 class="modal-title" id="exampleModalLongTitle">Политика конфиденциальности</h5>--}}
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{ $service->text }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>