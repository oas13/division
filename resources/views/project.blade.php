@extends('layout')

@section('content')
    <div class="container-fluid singleProject">
        <div class="row">
            <div class="col-md-5 proj-text">
                <h2>{{ $project->title }}</h2>
                <p>
                    {!! $project->text !!}
                </p>
                <div class="proj-pictograms">
                    @for($i = 0; $i < count($project->pictograms); $i++)
                        <img src="{{ asset($project->pictograms[$i]) }}" alt="" style="width: 60px;">
                    @endfor
                </div>
            </div>
            <div class="col-md-7">

                {{--{{ dump($project->toggle_galley) }}--}}
                <div class="carousel">
                    @if ($project->toggle_gallery)
                        @for($i = 0; $i < count($project->gallery); $i++)
                            <a href="{{ asset($project->gallery[$i]) }}" class="html5lightbox" data-width="960" data-height="720" style="border: 0;">
                                <img src="{{ asset($project->gallery[$i]) }}" alt="ew3GOgnHSEk"  title="ew3GOgnHSEk" />
                            </a>
                        @endfor
                            @if ($project->toggle_youtube)
                                <div class="video-wrapper yt-player">
                                    <iframe class="embed-player slide-media "
                                            src="{{ $project->youtube_src }}" height="520"
                                            frameborder="0" allowfullscreen>

                                    </iframe>
                                </div>
                            @endif
                    @else
                        <img src="{{ asset($project->image) }}" alt="" class="img-fluid">
                    @endif
                </div>
                </div>
                    <div class="review">
                        <a class="button showMore accordion">Отзыв заказчика</a>
                        <div class="panel">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque enim eveniet exercitationem
                            impedit, ipsa magni non perferendis praesentium provident quidem quod totam unde voluptatum.
                            A et inventore nobis rerum sunt.
                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection