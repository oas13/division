<div id="projects" class="section">
    <div class="container-fluid">
        <div class="row grid">
            @foreach($projects as $project)
                <div class="col-sm-1 col-md-6 col-lg-4 element-item hoverItem">
                    <a href="/projects/{{$project->slug}}">
                        <img src="{{ asset($project->image) }}" alt="" class="img-fluid">
                        <div class="back">
                            <p class="hoverTitle">{{ $project->title }}</p>
                        </div>
                    </a>
                </div>
            @endforeach
        </div>
        <a class="button showMore" id="showMoreProj">Посмотреть еще</a>
    </div>
</div>

