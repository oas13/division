@extends('layout')


@section('content')
    <div class="container-fluid topic">
        <div class="row">
            <div class="col-md-5 topic-text">
                <h2>{{ $topic->title }}</h2>
                <p>
                    {!! $topic->text !!}
                </p>
            </div>
            <div class="col-md-7">
                @if ($topic->toggle_gallery)
                    @if ($topic->toggle_youtube)
                        <iframe class="embed-player slide-media" width="980" height="520"
                                src="{{ $project->youtube_src }}"
                                frameborder="0">
                        </iframe>
                    @endif
                    @if ($topic->toggle_video)
                            <video class="slide-video slide-media" loop muted preload="metadata">
                                <source src="{{ $topic->video_src }}" type="video/mp4" />
                            </video>
                        @endif
                    <div class="carousel">
                        @for($i = 0; $i < count($topic->gallery); $i++)
                            <a href="{{ asset($topic->gallery[$i]) }}" class="html5lightbox" style="border: 0;">
                                <img src="{{ asset($topic->gallery[$i]) }}" alt="ew3GOgnHSEk"  title="ew3GOgnHSEk" />
                            </a>
                        @endfor
                    </div>
                @else
                    <img src="{{ asset($topic->image) }}" alt="" class="img-fluid">
                @endif
            </div>
            {{--<div class="review">--}}
            {{--<a class="button showMore accordion">Отзыв заказчика</a>--}}
            {{--<div class="panel">--}}
            {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque enim eveniet exercitationem--}}
            {{--impedit, ipsa magni non perferendis praesentium provident quidem quod totam unde voluptatum.--}}
            {{--A et inventore nobis rerum sunt.--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection