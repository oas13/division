<div id="about" class="section">
    <div class="row">
        <div class="col-md-6">
            <div class="team-text">
                {!! $about !!}
            </div>
        </div>
        <div class="col-md-6 col-lg-5" id="team-foto">
            <div class="row">
                @foreach($teamUsers as $teamUser)
                    <div class="col-md-6 team">
                        <img src="{{ $teamUser->image }}" alt="" class="img-fluid">
                        <p>
                            <span class="userName">{{ $teamUser->name }}</span><br>
                            <span style="font-size: 12px">{{ $teamUser->place }}</span>
                        </p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>